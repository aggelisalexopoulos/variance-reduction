rm(list=ls())
library(MASS)
library(mvtnorm)

source("~/PGstatic.R")

#G0
myG = function(x,j0,b0,b2,beta,gam_prime,gam_prime2,eps){
  gam_prime = exp(gam_prime)
  gam_prime2 = exp(gam_prime2)
  
  Gx_linear_plus = exp(beta*x[j0]-gam_prime2*sum(x^2))
  Gx_linear_minus = exp(-beta*x[j0]-gam_prime2*sum(x^2))
  
  Gx_quad_plus = exp(-gam_prime*(x[j0]-eps)^2- gam_prime*sum(x[-j0]^2))
  Gx_quad_minus = exp(-gam_prime*(x[j0]+eps)^2- gam_prime*sum(x[-j0]^2))
  
  Gx_linear = b0*Gx_linear_plus - b0*Gx_linear_minus
  Gx_quad =   b2*Gx_quad_plus - b2*Gx_quad_minus
  
  return(Gx_linear + Gx_quad)
}


#Give name of dataset
data_name = "Australian"
d=15
initial_inds=c(d,1:(d-1)) #auxiliary variable for importing the data


####MCMC settings
its =1000
burnin= 10000;thin =0

  if(d==3)c2 = 2.0
  if(d==8)c2=1.4
  if(d==14 | d==15) c2 =1.17
  if(d==25) c2 =0.95
  r = (1-c2/2)
  tau2 = c2/4

###auxiliary functions
sigmoid = function(x) {exp(x)/(1+exp(x))}

log_sigmoid = function(x){
  
  y = rep(0,length(x))
  idxPos = which(x>0)
  idxNeg = which(x<=0)
  y[idxPos] = -log(1+exp(-x[idxPos]))
  y[idxNeg] = x[idxNeg] - log(1+exp(x[idxNeg]))
  
  return(y)
  
}


#target
log_post=function(z){
  
  n = length(z) 
  Xz = myX%*%z
  log_lik = sum( log_sigmoid(tY*(Xz)) )

  grad = t(Y -sigmoid(Xz))%*%myX
  return(list(log_lik = log_lik,grad=grad))
}


myX = as.matrix(read.table(paste("~/data/Logistic_Data/Logistic_",data_name,"/myX",".txt",sep=""),sep=''))
myX=myX[,initial_inds] #put the columns in the right order
Y = as.matrix(read.table(paste("~/data/Logistic_Data/Logistic_",data_name,"/Y",".txt",sep=""),sep=''))
tY = 2*Y-1

####obtain MLE for the proposal
fit = glm(Y~myX-1,family = binomial("logit")  )
Sprop = vcov(fit)


#create matrices to store results
means = meanscv=rep(NA,d)
 y =x =matrix(NA, its,d)
alpha = rep(NA,its)
xcurrent = coefficients(fit)
compute_log_lik = log_post(xcurrent)
log_lik = compute_log_lik[[1]]
log_grad = as.vector(compute_log_lik[[2]])
###########run the chain for a burn-in period###############
for(i in 1:burnin){
  xprop = as.vector(xcurrent +0.5*c2*Sprop%*%log_grad+  mvrnorm(1,rep(0,d),c2*Sprop))
  compute_log_lik = log_post(xprop)
  log_lik_prop = compute_log_lik[[1]]
  log_grad_prop = as.vector(compute_log_lik[[2]])
  log_ratio = log_lik_prop-log_lik 
  prop_ratio = dmvnorm(xcurrent,xprop+0.5*c2*Sprop%*%log_grad_prop, c2*Sprop,log=T )
  prop_ratio = prop_ratio - dmvnorm(xprop,as.vector(xcurrent+0.5*c2*Sprop%*%log_grad), c2*Sprop,log=T )
  if(log(runif(1))< (log_ratio + prop_ratio) ){
    xcurrent = xprop
    log_lik = log_lik_prop
    log_grad = log_grad_prop
  }
}
###############################################
x[1,] =xcurrent #choose the last accepted point from burn-in to initialize the chain
acc=0 #counter for the acceptance rate
log_grad_store = matrix(NA,d,its)
log_grad_store[,1] = log_grad
#main MCMC loop

SpropInv = solve(Sprop)
for(i in 1:its){
  
  y[i,] = x[i,] +0.5*c2*Sprop%*%log_grad+ mvrnorm(1,rep(0,d),c2*Sprop) #propose y[i]
  z[i,] = x[i,] +0.5*c2*Sprop%*%log_grad
  compute_log_lik = log_post(y[i,])
  
  
  log_lik_prop = compute_log_lik[[1]]
  log_grad_prop = as.vector(compute_log_lik[[2]])
  
  log_ratio= log_lik_prop - log_lik #log-ratio of likelihood at proposed y[i] and current x[i]
  prop_ratio = dmvnorm(x[i,],y[i,]+0.5*c2*Sprop%*%log_grad_prop, c2*Sprop,log=T )
  prop_ratio = prop_ratio - dmvnorm(y[i,],x[i,]+0.5*c2*Sprop%*%log_grad, c2*Sprop,log=T )
  
  alpha[i] = min(1,exp(log_ratio+prop_ratio))
  if(log(runif(1))<(log_ratio+prop_ratio)){ 
    log_lik = log_lik_prop
    log_grad = log_grad_prop
    if(i<its){
      x[i+1,] =y[i,]
      log_grad_store[,i+1] =  log_grad_prop
    }
    acc=acc+1
  }else{
    if(i<its){
      x[i+1,] =x[i,]
      log_grad_store[,i+1] =  log_grad_store[,i]
    }
  }
  if(i%%10000==0)cat(c(i),"\n")#check number of iterations
}
cat(c(acc/its),"\n")

myx = x
S_tilde_org = cov(myx)

myS_tilde = S_tilde_org
for(dd in 1:d){ #post-process for variance reduction all the dimensions of the target
  
 #######permutation to apply Proposition 3
  if(dd==1){
    myind = 1:d
  }
  if(dd==d){
    myind = c(d,1:(d-1))
  }
  if(dd!=d & dd!=1){
    myind = c(dd,1:(dd-1),(dd+1):d)
  }
  
  
  x=myx[,myind]

mu_tilde = apply(x,2,mean)
S_tilde = Sprop[myind,myind]
S_tilde_inv = solve(S_tilde)
sample_L = t(chol(S_tilde))
sample_invL1 = solve(sample_L)


th = c(7.66393323, -14.80857329,   0.06130073,  -1.06688655,  -0.06475166,  -4.64607497)
Gx = hh = ff = atilde = rep(NA,its)
ysamples = xsamples = matrix(NA,dim(x)[1],dim(x)[2])

for(i in 1:its){
  xsamples[i,] = sample_invL1%*%(x[i,] - mu_tilde)
  ysamples[i,] = sample_invL1%*%(y[i,myind] - mu_tilde)
  Gx[i]=myG(xsamples[i,],1,th[1],th[2],th[3],th[4],th[6],th[5])
  Gdiff = (Gx[i] - myG(ysamples[i,],1,th[1],th[2],th[3],th[4],th[6],th[5]))/sample_invL1[1,1]
  
  Gauss_ratio = exp(-0.5*tau2*(  sum(ysamples[i,]^2)  -sum(xsamples[i,]^2)    ) ) 
  atilde[i] = min(1, Gauss_ratio)
  
  ff[i] =Gdiff*alpha[i]
  hh[i] = Gdiff*atilde[i]

}

#compute Eq[h]
myU= cv_notheta_comp(xsamples,1,th,r,mu_tilde,S_tilde,t(log_grad_store[myind,]))/sample_invL1[1,1]

#Estimate U=G-PG
U =  ff-hh+myU
Gx = Gx/sample_invL1[1,1]
PGhat=Gx-U

theta=(ergMean(x[-1,1]*(Gx[-1]+PGhat[-1]))-ergMean(x[-1,1])*ergMean(Gx[-1]+PGhat[-1])   )/ergMean((Gx[-1] - PGhat[1:(its-1)])^2)

means[dd] = mean(x[-1,1])
meanscv[dd] = mean(x[-1,1])-theta[its-1]*mean(U[-1])



}



