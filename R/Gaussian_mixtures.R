rm(list=ls())
library(MASS)
library(mvtnorm)





source("~/PG.R")

#G0
myG = function(x,j0,b0,b2,beta,gam_prime,gam_prime2,eps){
  gam_prime = exp(gam_prime)
  gam_prime2 = exp(gam_prime2)
  
  Gx_linear_plus = exp(beta*x[j0]-gam_prime2*sum(x^2))
  Gx_linear_minus = exp(-beta*x[j0]-gam_prime2*sum(x^2))
  
  Gx_quad_plus = exp(-gam_prime*(x[j0]-eps)^2- gam_prime*sum(x[-j0]^2))
  Gx_quad_minus = exp(-gam_prime*(x[j0]+eps)^2- gam_prime*sum(x[-j0]^2))
  
  Gx_linear = b0*Gx_linear_plus - b0*Gx_linear_minus
  Gx_quad =   b2*Gx_quad_plus - b2*Gx_quad_minus
  
  return(Gx_linear + Gx_quad)
}


h=2;d=10

S = as.matrix(read.table('~/data/myS10.txt'))


Sinv = solve(S)


####MCMC settings
MALA = FALSE
  c2 = 2.38^2/d
  r=kappa_c = 1
  tau_mala = 1

its =200000
burnin= 10000;
thin =0

###target
log_post = function(x,h){
  d = length(x)
  m1 = c(h/2,rep(0,d-1)); m2 = -c(h/2,rep(0,d-1))

  dens1 = dmvnorm(x,m1,S)
  dens2 = dmvnorm(x,m2,S)
  
  ffx = (1/2)*dens1 + (1/2)*dens2
  ff = log( ffx  )

  s1 = Sinv%*%(x-m1)%*%exp(-0.5*t(x-m1)%*%Sinv%*%(x-m1))
  s2 = Sinv%*%(x-m2)%*%exp(-0.5*t(x-m2)%*%Sinv%*%(x-m2))
  deriv =  -(2*pi)^(-d/2)*sqrt(det(Sinv))*((1/2)*s1+(1/2)*s2)/ffx
  
  return(list(log_lik=ff,grad = deriv))
}




#create matrices to store results
y =x =matrix(NA, its,d)
alpha = rep(NA,its)
xcurrent = rep(0,d)
Sprop = S
compute_log_lik = log_post(xcurrent,h)
log_lik = compute_log_lik[[1]]
log_grad = compute_log_lik[[2]]
###########run the chain for a burn-in period###############
for(i in 1:burnin){
  xprop = xcurrent +  mvrnorm(1,rep(0,d),c2*Sprop)
  compute_log_lik = log_post(xprop,h)
  log_lik_prop = compute_log_lik[[1]]
  log_grad_prop = compute_log_lik[[2]]
  log_ratio = log_lik_prop-log_lik
  if(log(runif(1))< log_ratio  ){
    xcurrent = xprop
    log_lik = log_lik_prop
    log_grad = log_grad_prop
  }
}
###############################################
x[1,] =xcurrent #choose the last accepted point from burn-in to initialize the chain
acc=0 #counter for the acceptance rate
SpropInv = solve(Sprop)

#main MCMC loop

for(i in 1:its){
  
  y[i,] = x[i,] + mvrnorm(1,rep(0,d),c2*Sprop) #propose y[i]
  compute_log_lik = log_post(y[i,],h)
  log_lik_prop = compute_log_lik[[1]]
  log_grad_prop = compute_log_lik[[2]]
  
  log_ratio= log_lik_prop - log_lik #log-ratio of likelihood at proposed y[i] and current x[i]
  alpha[i] = min(1,exp(log_ratio))
  if(log(runif(1))<log_ratio){ 
    log_lik = log_lik_prop
    log_grad = log_grad_prop
    if(i<its){
      x[i+1,] =y[i,]
      log_lik_store[i+1] =log_lik
      log_grad_store[,i+1] =  log_grad_prop
    }
    acc=acc+1
  }else{
    if(i<its){
      x[i+1,] =x[i,]
      log_lik_store[i+1] = log_lik_store[i]
      log_grad_store[,i+1] =  log_grad_store[,i] 
    }
  }

  if(i%%10000==0)cat(c(i),"\n")#check number of iterations
}
cat(c(reps,acc/its),"\n")


mu_tilde = apply(x,2,mean)
S_tilde = Sprop
S_tilde_inv = solve(S_tilde)
sample_L = t(chol(S_tilde))
sample_invL1 = solve(sample_L)  

Gx = hh = ff = atilde = rep(NA,its)
ysamples = xsamples = matrix(NA,dim(x)[1],dim(x)[2])
th=c(8.7078445,  -3.5619307,   0.2916344,  -2.1799684,   3.9162519, -12.9526549)

######post -processing for variance reduction
for(i in 1:its){
    
  xsamples[i,] = sample_invL1%*%(x[i,] - mu_tilde)#standardize samples
  ysamples[i,] = sample_invL1%*%(y[i,] - mu_tilde)#standardize samples
  Gx[i]=myG(xsamples[i,],1,th[1],th[2],th[3],th[4],th[6],th[5])
  Gdiff = (Gx[i] - myG(ysamples[i,],1,th[1],th[2],th[3],th[4],th[6],th[5]))/sample_invL1[1,1]
  
  Gauss_ratio = exp(-0.5*(  sum(ysamples[i,]^2)  -sum(xsamples[i,]^2)    ) ) 
  atilde[i] = min(1, Gauss_ratio)
  
  ff[i] =Gdiff*alpha[i]
  hh[i] = Gdiff*atilde[i]
  
  
}

#compute Eq[h]
myU= cv_notheta_comp(xsamples,1,th,r)/sample_invL1[1,1]

#Estimate U
U =  ff-hh+myU
Gx = Gx/sample_invL1[1,1]
PGhat=Gx-U

theta=(ergMean(x[-1,1]*(Gx[-1]+PGhat[-1]))-ergMean(x[-1,1])*ergMean(Gx[-1]+PGhat[-1])   )/ergMean((Gx[-1] - PGhat[1:(its-1)])^2)

means = mean(x[-1,1])
meanscv = mean(x[-1,1])-theta[its-1]*mean(U[-1])












